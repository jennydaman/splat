CC=gcc

Criterion=Criterion/build

LDLIBS=-lcriterion
LDFLAGS=-L$(Criterion)

TINY_SHA3=sha3.o
INCLUDE_DIRS=-I. -ICriterion/include -Itiny_sha3

SPLAT=splat.o
SRC=splat.c
SPEC=test/test_splat.c
SPEC_OBJECT=test_splat.o
DRIVER=run_tests
REPORT=report.xml

$(SPLAT): $(TINY_SHA3)
	$(CC) -o $@ -c $(SRC) $(INCLUDE_DIRS)

$(TINY_SHA3): tiny_sha3/sha3.c
	$(CC) -o $@ -c $^

build: $(SPLAT)

test: $(REPORT)

$(REPORT): $(DRIVER)
	LD_LIBRARY_PATH=$(Criterion):$(LD_LIBRARY_PATH) ./$< --verbose --xml=$@

$(Criterion):
	mkdir $@ && cd $@ && cmake .. && cmake --build .

$(SPEC_OBJECT): $(SPEC) $(Criterion)
	$(CC) -c $< -o $@ $(INCLUDE_DIRS)

$(DRIVER): $(SPLAT) $(TINY_SHA3) $(SPEC_OBJECT)
	$(CC) -o $@ $^ $(LDLIBS) $(LDFLAGS)

.PHONY: build test clean
.PRECIOUS : $(Criterion)
.INTERMEDIATE: $(DRIVER) $(RUN_TESTS)

clean:
	rm $(SPLAT) $(TINY_SHA3) $(SPEC_OBJECT) $(REPORT)
