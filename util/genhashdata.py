#!/usr/bin/env python3

from random import randint
import sys
from hashlib import sha3_224

SECRET_LENGTH = 20
SALT_LENGTH = 20
TOKEN_LENGTH = 28

def array2clang(array):
    return '{%s}' % ', '.join([hex(a) for a in array])

def random_bytearray(length):
    return [randint(0, 255) for a in range(length)]

secret = random_bytearray(SECRET_LENGTH)
salt = random_bytearray(SALT_LENGTH)

secret = [
    0xef, 0x40, 0x07, 0xf6, 0xb5, 0xda, 0x8b, 0x1c, 0x2a, 0x88,
    0x70, 0x4b, 0x0c, 0x32, 0x11, 0x3e, 0x16, 0xcf, 0x4c, 0xc3
]
# result of srand(1000)
# 182 91 137 158 116 37 187 114 188 127 114 195 226 251 49 113 112 44 155 80
salt = [
    0xb6, 0x5b, 0x89, 0x9e, 0x74, 0x25, 0xbb, 0x72, 0xbc, 0x7f,
    0x72, 0xc3, 0xe2, 0xfb, 0x31, 0x71, 0x70, 0x2c, 0x9b, 0x50
]

print(f'secret: {array2clang(secret)}')
print(f'salt:   {array2clang(salt)}')

m = sha3_224()
m.update(bytearray(secret))
m.update(bytearray(salt))

print('---')
print(f'result: {array2clang(m.digest())}')
