#!/usr/bin/env python3

from random import randint
import sys

m = int(sys.argv[1]) if len(sys.argv) > 1 else 32
print('{%s}' % ', '.join([hex(randint(0, 255)) for a in range(m)]))
