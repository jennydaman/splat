#ifndef SPLAT_H
#define SPLAT_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#define SALT_LENGTH 20
#define TOKEN_LENGTH 28  // 224 bits
#define SECRET_LENGTH 20

/*
 * https://www.notion.so/2796574412424405adbde9136d6f750d?v=c402dc00224445c0855ca2a5c9bcb4d8
 */
enum Command {
    NONE = 0x00,
    UNLOCK = 0x01,
    RESET = 0x80,
    PASSWORD = 0x81,
    SCOOTER_CLEAR = 0x82,
    SCOOTER_ASSIGN = 0x83,
    UNKNOWN,
    WRONG,
    ERROR
};

/*
 * Deep copy of contents from input including and after index 1
 * to the existing array variable.
 */
void set_array(uint8_t* variable, size_t variable_len, uint8_t* input, size_t len);

/*
 * Returns pointer to the salt
 */
uint8_t* get_salt();

/*
 * Supply random number generator with some entropy.
 */
void seed_rng(unsigned int seed);

/*
 * The first byte of input represents a command,
 * then the rest represents a payload.
 * 
 * If the payload is valid, an action is performed and the command is returned.
 * MCU needs to repond to whatever result is returned by this function.
 * 
 * NONE            => do nothing
 * UNLOCK          => activate motor
 * RESET           => factory reset
 * PASSWORD        => write payload to EEPROM, then call seed_rng() to prepare the first salt
 * SCOOTER_CLEAR   => disable Bluetooth advertising, remove scooterID from EEPROM
 * SCOOTER_ASSIGN  => check if scooterID is empty, then determine ADV payload,
 *                    write new scooterID to EEPROM, and enable Bluetooth advertising
 */
enum Command handle(uint8_t* input, size_t len);

#endif
