#include "splat.h"
#include "sha3.h"

// TODO use cryptographically secure random number generator
#include <stdlib.h>

uint8_t secret [SECRET_LENGTH];
uint8_t salt [SALT_LENGTH];
uint8_t token [TOKEN_LENGTH];
// note: scooter ID should be stored by MCU

// true when secret is set.
bool ready = false;


// utility function
void set_array(uint8_t* variable, size_t variable_len, uint8_t* input, size_t len) {
    len -= 1;
    if (variable_len > len)
        variable_len = len;
    for (int i = 0; i < variable_len; i++)
        variable[i] = input[i + 1];
}


uint8_t* get_salt() {
    return salt;
}


void random_salt() {
    for (int i = 0; i < SALT_LENGTH; i++)
        salt[i] = rand() % 256;
}


/*
 * token <-- sha3(secret + salt)
 */
bool hash() {
    sha3_ctx_t sha3;
    sha3_init(&sha3, TOKEN_LENGTH);
    sha3_update(&sha3, secret, SECRET_LENGTH);
    sha3_update(&sha3, salt, SALT_LENGTH);
    sha3_final(token, &sha3);
};


/*
 * Generate a new salt then compute the hash.
 */
bool next_token() {
    random_salt();
    hash(salt);
}


// stir
void seed_rng(unsigned int seed) {
    srand(seed);
    next_token();
}


/*
 * Byte-by-byte comparison of two arrays but with an offset for the input array
 * since the first byte represents a command, and the bytes
 * including and after index 1 contain the payload.
 */
bool compare_bytes(uint8_t* correct, size_t correct_len, uint8_t* input, size_t len) {
    if (len <= correct_len)
        return false;
    for (int i = 0; i < correct_len; i++) {
        if (input[i + 1] != correct[i])
            return false;
    }
    return true;
}


/*
 * Validates the payload against the current token.
 * Regardless of successful or not, a new salt/token is created.
 */
bool check_token(uint8_t* input, size_t len) {
    if (token == NULL)
        return false;
    bool result = compare_bytes(token, TOKEN_LENGTH, input, len);
    next_token();
    return result;
}


/*
 * Simply checks that the payload matches the current token.
 */
enum Command attempt_scooter(uint8_t* input, size_t len) {
    return check_token(input, len) ? SCOOTER_CLEAR : WRONG;
}


/*
 * If the token is correct, then returns UNLOCK.
 */
enum Command attempt_unlock(uint8_t* input, size_t len) {
    if (!ready)
        return ERROR;
    return check_token(input, len) ? UNLOCK : WRONG;
}


/*
 * If the payload matches the secret, then the secret in memory is wiped.
 */
enum Command attempt_reset(uint8_t* input, size_t len) {
    if (!compare_bytes(secret, SECRET_LENGTH, input, len))
        return WRONG;
    ready = false;
    for (int i = 0; i < SECRET_LENGTH; i++)
        secret[i] = 0;
    return RESET;
}

/*
 * If the secret is not currently set in memory, then copy the payload into secret
 * and generate the first salt/token.
 */
enum Command attempt_set_secret(uint8_t* input, size_t len) {
    if (ready)
        return ERROR;
    set_array(secret, SECRET_LENGTH, input, len);
    ready = true;
    return PASSWORD;
}


enum Command handle(uint8_t* input, size_t len) {
    if (len < 1)
        return UNKNOWN;
    switch (input[0]) {
        case NONE:
            return NONE;
        case UNLOCK:
            return attempt_unlock(input, len);
        case RESET:
            return attempt_reset(input, len);
        case PASSWORD:
            return attempt_set_secret(input, len);
        case SCOOTER_CLEAR:
            return attempt_scooter(input, len);
        case SCOOTER_ASSIGN:
            return SCOOTER_ASSIGN;
    }
    return UNKNOWN;
}
