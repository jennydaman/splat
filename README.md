# Introduction

We present "*Symmetric PasswordLess Authentication Tokens*" (or *SPLAT* for short). It is a communication strategy to ensure authenticity of short messages over an insecure channel between two devices using a shared *secret*. The protocol is intended for use over Bluetooth LE and was developed for [Mount Locks](http://www.mountlocks.com/).


<details><summary>Background</summary>

In IoT, cybersecurity is often overlooked [1]. Our objective is to propose a way to communicate which is theoretically resilient to hacking. The effort to compromise a Mount Locks device by exploiting its communication should be no more feasible than physical destruction of the lock device.

A protocol should be platform agnostic, which guarantees failsafe in cases where the transport layer protocols are vulnerable. Should Bluetooth pairing be compromised [2], our protocol should still at least guarantee confidentiality and integrity.

[1] [Booth LK, Mayrany M. (2019). "IoT Penetration Testing: Hacking an Electric Scooter"](https://www.kth.se/polopoly_fs/1.914061.1561621457!/Louis) 

[2] [Zegeye WK. (2015). "Exploiting Bluetooth Low Energy Pairing Vulnerability in Telemedicine."](https://www.morgan.edu/Documents/ACADEMICS/DEPTS/ElectricEng/Exploiting_BluetoothLE_final.pdf) 

## Threat Model

The naive approach is to use a password. We can assume the *secret* (password) is first initialized over a secure channel. Both *lock* and *phone* know *secret*.

A **man-in-the-middle** (MitM) is a step up from passive eavesdropping. Here is an example of a MitM spoofing attack:

1. *lock* broadcasts its payload *ADV* over the Bluetooth channel.
2. *attacker* reads *ADV*
3. *attacker* copies *ADV* and broadcasts *ADV* (spoof)
4. *phone* connects to *attacker* (oops)
5. *phone* sends *secret* to *attacker*
6. *attacker* sends *secret* to *lock*

</details>

# Overview


**One-time *tokens*** are generated. **The *secret* is never sent between *central* and *peripheral* devices.**

1. *Alice* generates a new `salt` (which is bytes of random data)
2. *Alice* sends `salt` to *Bob*
3. Both *Alice* and *Bob* compute: `token ← hash(salt + secret)`
4. *Bob* sends `token + command` to *Alice*
5. *Alice* verifies `my.token = their.token`

<details><summary>Actors</summary>

- *Alice* represents the peripheral Bluetooth device, the Mount Locks device.
- *Bob* represents the central Bluetooth device, the user's smartphone.
</details>

<details><summary>Analysis</summary>

Some information can be revealed by eavesdropping.

- *salt*
- *token*
- Mount Locks specific [GATT profile](https://www.notion.so/Firmware-Description-0a4a861895664e7ead2f030ca0a96ed5#c1d6f2ae2f924a439d06c9afdbc85ff4)

All of which is intended to be public, non-sensitive data.

*secret* cannot be cracked from knowing the *salt* and *token*. The *token* works once and never again, so replicating the token is pointless. Essentially, both the *salt* and *token* are basically random noise to eavesdroppers.

*secret* should never be transmitted between devices under ordinary operation!
</details>

# Sequence Diagram

![Sequence Diagram](docs/sequence_diagram.svg)

# Build

1. `git submodule update --init`
2. `make`

## Run Tests

1. `make test`

# Documentation

[Documentation on Notion](https://www.notion.so/Firmware-Description-0a4a861895664e7ead2f030ca0a96ed5#32e0569678ff473784b42590081fc575)

#### `input`

`input` is a byte array read from the `0x4B66` (RX) characteristic.

#### payload

*Payload* denotes the data after the first byte of `input`.
