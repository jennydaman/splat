#include "criterion/criterion.h"
#include "splat.h"

#include <time.h>

uint8_t set_secret[21] = {
    PASSWORD,
    0xef, 0x40, 0x07, 0xf6, 0xb5, 0xda, 0x8b, 0x1c, 0x2a, 0x88,
    0x70, 0x4b, 0x0c, 0x32, 0x11, 0x3e, 0x16, 0xcf, 0x4c, 0xc3
};
uint8_t wipe_secret[21] = {
    RESET,
    0xef, 0x40, 0x07, 0xf6, 0xb5, 0xda, 0x8b, 0x1c, 0x2a, 0x88,
    0x70, 0x4b, 0x0c, 0x32, 0x11, 0x3e, 0x16, 0xcf, 0x4c, 0xc3
};
uint8_t guess[29] = {
    UNLOCK,
    0xaf, 0xef, 0x43, 0x21, 0xed, 0x1b, 0xe5,
    0x84, 0xb2, 0xa6, 0x5a, 0x79, 0xf5, 0x3d,
    0x99, 0x95, 0xed, 0xb3, 0x9b, 0xc8, 0xa9,
    0x37, 0x1e, 0x30, 0xfb, 0x19, 0x18, 0x63
};
int len = TOKEN_LENGTH + 1;

void setup(void) {
    handle(set_secret, SECRET_LENGTH + 1);
    seed_rng(time(NULL));
}

void teardown(void) {
    handle(wipe_secret, SECRET_LENGTH + 1);
}

uint8_t* deep_copy(uint8_t* orig, size_t len) {
    uint8_t* copy = malloc(len * sizeof(*copy));
    for (int i = 0; i < len; i++)
        copy[i] = orig[i];
    return copy;
}


Test(state_machine, secret) {
    enum Command result = handle(guess, len);
    cr_assert_eq(result, ERROR, "did not return ERROR when trying to unlock before secret was even set.");
    result = handle(set_secret, SECRET_LENGTH + 1);
    cr_assert_eq(result, PASSWORD, "PASSWORD broken, could not set the secret.");

    uint8_t bad_secret[21] = {
        PASSWORD,
        0x53, 0xed, 0x11, 0x6c, 0x16, 0xe8, 0xab, 0x97, 0xa9, 0x2b,
        0x0e, 0x19, 0xb0, 0x02, 0xe1, 0xae, 0x53, 0x72, 0x65, 0xfa
    };
    result = handle(bad_secret, 21);
    cr_assert_eq(result, ERROR, "Command.PASSWORD did not produce ERROR when secret does not match.");
    bad_secret[0] = RESET;
    result = handle(bad_secret, 21);
    cr_assert_neq(result, PASSWORD, "was allowed to overwrite secret.");

    result = handle(wipe_secret, SECRET_LENGTH + 1);
    cr_assert_eq(result, RESET, "Command.RESET broken, could not wipe the secret.");
    result = handle(guess, len);
    cr_assert_eq(result, ERROR, "did not return ERROR when attempting UNLOCK right after RESET.");
}

TestSuite(general, .init = setup, .fini = teardown);

Test(general, none_command, .description = "Command.NONE should do nothing, salt should not change") {
    uint8_t* salt = get_salt();
    uint8_t* orig_salt = deep_copy(salt, SALT_LENGTH);
    uint8_t none_command[3] = {NONE, 0, 2}; // trailing bytes don't matter, are ignored
    enum Command result = handle(none_command, 3);
    cr_assert_eq(result, NONE, "Command.NONE is broken.");
    cr_assert_arr_eq(orig_salt, salt, SALT_LENGTH, "salt was changed by Command.NONE");
}

Test(general, unlock_command) {
    enum Command result = handle(guess, len);
    cr_assert_eq(result, WRONG, "unauthorized UNLOCK or random salt/token collision");
    seed_rng(1000);  // recomputes hash
    uint8_t correct_hash[29] = {
        UNLOCK,
        0xc4, 0x10, 0x97, 0xc0, 0xeb, 0x3d, 0x63,
        0xc0, 0xbf, 0x3a, 0xe1, 0x45, 0x6e, 0x30,
        0x95, 0xe4, 0x4a, 0x62, 0xce, 0x7c, 0xab,
        0x1f, 0x98, 0x4a, 0xc1, 0x6f, 0xe8, 0xf3
    };
    result = handle(correct_hash, len);
    cr_assert_eq(result, UNLOCK, "seeded hash did not pass check.");
    result = handle(correct_hash, len);
    cr_assert_eq(result, WRONG, "unchanged token worked twice in a row.");
}
